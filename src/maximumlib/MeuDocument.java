package maximumlib;

import java.text.Normalizer;
import javax.swing.JTextField;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

public class MeuDocument extends PlainDocument {

    private final JTextField jTextField;

    public MeuDocument(JTextField jTextField) {
        super();
        this.jTextField = jTextField;
    }

    @Override
    public void insertString(int offset, String str, AttributeSet attr) throws BadLocationException {

        String string = jTextField.getText();
        int columns = jTextField.getColumns(); // limite de string
        boolean doubleBuffered = jTextField.isDoubleBuffered(); // seta um campos numerico
        boolean dragEnabled = jTextField.getDragEnabled(); // para campos sem acentos e com letas menusculas, caixa baixa
        boolean autoscrolls = jTextField.getAutoscrolls(); // para campos somente letras maiscúlas / caixa alta

        // TXT PADRAO DE TEXTO -> RETORNA SE NECESSÁRIO STRING LIMITADAS, MAIUSCULAS OU SEM ACENTOS
        if (!doubleBuffered) {

            // retorna a string sem acentos e com letas menusculas, caixa baixa, caso setado nas configurações
            if (dragEnabled) {
                String unaccented = Normalizer.normalize(str, Normalizer.Form.NFD);
                str = unaccented.replaceAll("[^\\p{ASCII}]", "");
                str = str.toLowerCase();
            }

            // retorna a string em letra maiúscula / caixa alta
            if (!autoscrolls) {
                str = str.toUpperCase();
            }

            if (columns > 0) {
                // retorna string de texto com limite de columns
                if ((getLength() + str.length()) > columns) {
                    str = null;
                }
            }

            // retorno da string
            super.insertString(offset, str, attr);
        }

        // TXT MARCADA COMO DOUBLE -> RETORNA VALORES NUMERICO INCLUINDO UMA VIRGULA E SINAL DE MENOS (NEGATIVO)
        if (doubleBuffered) {
            boolean isInteger = true;
            for (int i = 0; i < str.length(); i++) {
                isInteger = Character.isDigit(str.charAt(i));
            }

            if (isInteger || str.contains(",") || str.contains("-")) {
                // retorno da string
                super.insertString(offset, str, attr);
            }
        }

    }
}
