/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package maximumlib;

import java.awt.Color;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.Normalizer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.Random;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.text.MaskFormatter;

/**
 *
 * @author Anderson
 */
public class Metodos {

    private static Metodos instancia;
    public static String valorTempCampo = "";

    public static Metodos getInstancia() {
        if (instancia == null) {
            instancia = new Metodos();
        }
        return instancia;
    }

    public void excluiTodosArquivosDeUmaPasta(String destino) {
        try {
            File pasta = new File(destino);
            if (pasta.isDirectory()) {
                File[] arquivos = pasta.listFiles();
                for (File arquivo : arquivos) {
                    arquivo.delete();
                }
            }

        } catch (Exception e) {
            System.out.println("erro ao excluir arquivos de uma pasta: " + e.toString());
        }
    }

    public void abrirArquivoComputador(File arquivo) throws IOException {

        Desktop.getDesktop().open(arquivo);

    }

    public void excluiArquivoComputador(File arquivo) throws Exception {
        try {
            arquivo.delete();
        } catch (Exception e) {
            throw new Exception("Não foi possível excluir o arquivo: " + arquivo.getName() + "!");
        }
    }

    public void abrirUrlNavegador(String url) {
        try {
            Desktop.getDesktop().browse(new URI(url));

        } catch (Exception e) {
            System.out.println("erro ao abrir url em navegador: " + e.toString());
        }
    }

    public void tempoSleep(double seg) {
        try {
            // tempo em milesegundos
            double tempo = (seg * 1000);
            Thread.currentThread().sleep((long) tempo);

        } catch (InterruptedException e) {
            System.out.println("interrupção no tempo sleep: " + e.toString());
        }
    }

    public String chaveSistema() {
        Locale local = new Locale("pt", "BR");
        DateFormat formatador = new SimpleDateFormat("MMMMyyyy", local);
        return formatador.format(new Date()).toString();
    }

    public String raizDoSistema() {
        return System.getProperty("user.dir").replace("\\dist", "").replace("/dist", "");
    }

    public String senhaAcesso() {
        Date data = new Date();
        SimpleDateFormat formatador = new SimpleDateFormat("MMdd");
        return formatador.format(data).toString();
    }

    public String retornarHostName() {
        try {
            java.net.InetAddress inet = java.net.InetAddress.getLocalHost();
            return inet.getHostName();

        } catch (Exception e) {
            return "localhost";
        }
    }

    public void preparaDocument(Object objeto) {

        // caso o objeto venha um JTextField trabalha o mesmo com os eventos e parametros document necessarios 
        if (objeto instanceof JTextField) {

            // captura um JTextField passando o mesmo para a class MeuDocument que extende da PlainDocument responsável por gerenciamento de strings
            JTextField jTextField = ((JTextField) objeto);

            // adiciona e remove o cifrao nos campos jTextField com a propriedade name setada com R$
            if (jTextField.getName() == "R$") {
                jTextField.addFocusListener(new FocusListener() {

                    @Override
                    public void focusGained(FocusEvent arg0) {
                        if (jTextField.isEditable()) {
                            valorTempCampo = jTextField.getText();
                            jTextField.setText("");
                        }
                    }

                    @Override
                    public void focusLost(FocusEvent arg0) {
                        if (jTextField.getText().isEmpty()) {
                            jTextField.setText(valorTempCampo);
                        }

                        // para valor negativo
                        if (jTextField.getText().contains("-")) {
                            if (!jTextField.getText().contains("-R$")) {
                                jTextField.setText("-R$ " + (jTextField.getText().replace("-", "")));
                            }

                            // para valor positivo
                        } else {
                            if (!jTextField.getText().contains("R$")) {
                                jTextField.setText("R$ " + jTextField.getText());
                            }

                        }
                    }
                });
            }

            // adiciona e remove o porcento nos campos jTextField com a propriedade name setada com %
            if (jTextField.getName() == "%") {
                jTextField.addFocusListener(new FocusListener() {

                    @Override
                    public void focusGained(FocusEvent arg0) {
                        if (jTextField.isEditable()) {
                            valorTempCampo = jTextField.getText();
                            jTextField.setText("");
                        }
                    }

                    @Override
                    public void focusLost(FocusEvent arg0) {
                        if (jTextField.getText().isEmpty()) {
                            jTextField.setText(valorTempCampo);
                        }
                        if (!jTextField.getText().contains("%")) {
                            jTextField.setText(jTextField.getText() + "%");
                        }
                    }
                });
            }

            // adiciona e remove o porcento nos campos jTextField com a propriedade name setada com %
            if (jTextField.getName() == "Kg") {
                jTextField.addFocusListener(new FocusListener() {

                    @Override
                    public void focusGained(FocusEvent arg0) {
                        if (jTextField.isEditable()) {
                            valorTempCampo = jTextField.getText();
                            jTextField.setText("");
                        }
                    }

                    @Override
                    public void focusLost(FocusEvent arg0) {
                        if (jTextField.getText().isEmpty()) {
                            jTextField.setText(valorTempCampo);
                        }
                        if (!jTextField.getText().contains(" Kg")) {
                            jTextField.setText(jTextField.getText() + " Kg");
                        }
                    }
                });
            }

            // adiciona e remove o porcento nos campos jTextField com a propriedade name setada com %
            if (jTextField.getName() == "0") {
                jTextField.addFocusListener(new FocusListener() {

                    @Override
                    public void focusGained(FocusEvent arg0) {
                        if (jTextField.isEditable()) {
                            valorTempCampo = jTextField.getText();
                            jTextField.setText("");
                        }
                    }

                    @Override
                    public void focusLost(FocusEvent arg0) {
                        if (jTextField.getText().isEmpty()) {
                            jTextField.setText(valorTempCampo);
                        }
                    }
                });
            }

            // aplica as regras de escrita dos campos de texto conforme descricao em name do campo
            String string = jTextField.getText();
            jTextField.setDocument(new MeuDocument(jTextField));
            jTextField.setText(string);

        }

        // caso o objeto venha um JPanel percorre por o metodo por recursao procurando os JTextField existentes
        if (objeto instanceof JPanel) {
            for (Component c : ((JPanel) objeto).getComponents()) {
                preparaDocument(c);
            }
        }

        // caso o objeto venha um JTabbedPane percorre por o metodo por recursao procurando os JTextField existentes
        if (objeto instanceof JTabbedPane) {
            for (Component c : ((JTabbedPane) objeto).getComponents()) {
                preparaDocument(c);
            }
        }
    }

    public Date capturaDataDoCampo(Date data) {
        try {
            return data;
        } catch (Exception e) {
            return null;
        }
    }

    public int diferencaDeDiasEntreDatas(Date dataInicial, Date dataFinal) {
        int qtdDias = 0;
        try {
            long dt = (dataFinal.getTime() - dataInicial.getTime());
            long dias = (dt / 86400000L);
            qtdDias = (int) dias;

        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return qtdDias;
    }

    public boolean validaNumeroDoCPFOuCNPJ(String cpfCnpj) {
        try {
            boolean validacao = true;
            if (!cpfCnpj.isEmpty()) {
                if (cpfCnpj.length() > 11) {
                    validacao = validaNumeroDoCNPJ(cpfCnpj);
                } else {
                    validacao = validaNumeroDoCPF(cpfCnpj);
                }
            }
            return validacao;

        } catch (Exception e) {
            return false;
        }
    }

    public boolean validaNumeroDoCNPJ(String numeroCNPJ) {
        try {
            if (!numeroCNPJ.isEmpty()) {
                // elimita os caracteres especiais do CNPJ informado
                String cnpjSomenteNumeros = numeroCNPJ.replaceAll("[^0-9]", "");
                numeroCNPJ = cnpjSomenteNumeros;

                int soma = 0, dig;
                String cnpj_calc = numeroCNPJ.substring(0, 12);

                if (numeroCNPJ.length() != 14) {
                    return false;
                }

                char[] chr_cnpj = numeroCNPJ.toCharArray();

                // Primeira parte  
                for (int i = 0; i < 4; i++) {
                    if (chr_cnpj[i] - 48 >= 0 && chr_cnpj[i] - 48 <= 9) {
                        soma += (chr_cnpj[i] - 48) * (6 - (i + 1));
                    }
                }
                for (int i = 0; i < 8; i++) {
                    if (chr_cnpj[i + 4] - 48 >= 0 && chr_cnpj[i + 4] - 48 <= 9) {
                        soma += (chr_cnpj[i + 4] - 48) * (10 - (i + 1));
                    }
                }
                dig = 11 - (soma % 11);

                cnpj_calc += (dig == 10 || dig == 11) ? "0" : Integer.toString(dig);

                // Segunda parte  
                soma = 0;
                for (int i = 0; i < 5; i++) {
                    if (chr_cnpj[i] - 48 >= 0 && chr_cnpj[i] - 48 <= 9) {
                        soma += (chr_cnpj[i] - 48) * (7 - (i + 1));
                    }
                }
                for (int i = 0; i < 8; i++) {
                    if (chr_cnpj[i + 5] - 48 >= 0 && chr_cnpj[i + 5] - 48 <= 9) {
                        soma += (chr_cnpj[i + 5] - 48) * (10 - (i + 1));
                    }
                }
                dig = 11 - (soma % 11);
                cnpj_calc += (dig == 10 || dig == 11) ? "0" : Integer.toString(dig);

                return numeroCNPJ.equals(cnpj_calc);
            } else {
                // CNPJ em branco está valido
                return true;
            }

        } catch (Exception e) {
            return false;
        }
    }

    public boolean validaNumeroDoCPF(String numeroCPF) {
        try {
            if (!numeroCPF.isEmpty()) {
                // elimita os caracteres especiais do CPF informado
                String cpfSomenteNumeros = numeroCPF.replaceAll("[^0-9]", "");
                numeroCPF = cpfSomenteNumeros;

                int d1, d2;
                int digito1, digito2, resto;
                int digitoCPF;
                String nDigResult;

                d1 = d2 = 0;
                digito1 = digito2 = resto = 0;

                for (int nCount = 1; nCount < numeroCPF.length() - 1; nCount++) {
                    digitoCPF = Integer.valueOf(numeroCPF.substring(nCount - 1, nCount))
                            .intValue();

                    // multiplique a ultima casa por 2 a seguinte por 3 a seguinte por 4
                    // e assim por diante.
                    d1 = d1 + (11 - nCount) * digitoCPF;

                    // para o segundo digito repita o procedimento incluindo o primeiro
                    // digito calculado no passo anterior.
                    d2 = d2 + (12 - nCount) * digitoCPF;
                }
                ;

                // Primeiro resto da divisão por 11.
                resto = (d1 % 11);

                // Se o resultado for 0 ou 1 o digito é 0 caso contrário o digito é 11
                // menos o resultado anterior.
                if (resto < 2) {
                    digito1 = 0;
                } else {
                    digito1 = 11 - resto;
                }

                d2 += 2 * digito1;

                // Segundo resto da divisão por 11.
                resto = (d2 % 11);

                // Se o resultado for 0 ou 1 o digito é 0 caso contrário o digito é 11
                // menos o resultado anterior.
                if (resto < 2) {
                    digito2 = 0;
                } else {
                    digito2 = 11 - resto;
                }

                // Digito verificador do CPF que está sendo validado.
                String nDigVerific = numeroCPF.substring(numeroCPF.length() - 2,
                        numeroCPF.length());

                // Concatenando o primeiro resto com o segundo.
                nDigResult = String.valueOf(digito1) + String.valueOf(digito2);

                // comparar o digito verificador do cpf com o primeiro resto + o segundo
                // resto.
                return nDigVerific.equals(nDigResult);
            } else {
                // CPF em branco está valido
                return true;
            }

        } catch (Exception e) {
            return false;
        }
    }

    public String mascaraDaString(String value, String pattern) {
        try {
            MaskFormatter mf = new MaskFormatter(pattern);
            mf.setValueContainsLiteralCharacters(false);
            return mf.valueToString(value);

        } catch (ParseException e) {
            return value;
        }
    }

    public void abreJanelaEmAreaNoCentro(JInternalFrame janela, JDesktopPane area) {
        try {
            int larguraPainel = area.getWidth();
            int alturaPainel = area.getHeight();
            int larguraJanela = janela.getWidth();
            int alturaJanela = janela.getHeight();

            janela.setLocation(larguraPainel / 2 - larguraJanela / 2, alturaPainel / 2 - alturaJanela / 2);
            area.add(janela);
            janela.setVisible(true);
            janela.toFront();

        } catch (Exception e) {
            System.out.println("erro ao centralizar janela: " + e.toString());
        }
    }

    public void lookAndFeel() {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());

        } catch (Exception e) {
            System.out.println("erro lookAndFeel: " + e.toString());
        }
    }

    public String converteDataParaStringFormato(Date data, String formato) {
        try {
            Locale local = new Locale("pt", "BR");
            DateFormat formatador = new SimpleDateFormat(formato, local); // dd/MM/yyyy HH:mm:ss -> obs: mes por extenso -> MMMM
            return formatador.format(data).toString();

        } catch (Exception e) {
            return null;
        }
    }

    public Date converteStringFormatoParaDate(String dado, String formato) {
        try {
            Locale local = new Locale("pt", "BR");
            DateFormat formatador = new SimpleDateFormat(formato, local); // dd/MM/yyyy HH:mm:ss -> obs: mes por extenso -> MMMM
            Date data = (Date) formatador.parse(dado);
            return data;

        } catch (Exception e) {
            return null;
        }
    }

    public Double converteStringValorParaDouble(String valor) {
        try {
            DecimalFormatSymbols dfs = new DecimalFormatSymbols(new Locale("pt", "BR"));
            DecimalFormat df = new DecimalFormat("#,##0.00", dfs);
            return df.parse(valor).doubleValue();

        } catch (Exception e) {
            return 0.00;
        }
    }

    public String converteDoubleParaStringValor(Double valor) {
        try {
            DecimalFormatSymbols moeda = new DecimalFormatSymbols(new Locale("pt", "BR"));
            DecimalFormat df = new DecimalFormat("#,##0.00", moeda);
            return df.format(valor);

        } catch (Exception e) {
            return "0,00";
        }
    }

    public Double converteStringMoedaParaDouble(String valor) {
        try {
            // captura o valor temporario do campo, pois o evento onfocus limpa o valor inicial do campo, fazendo com que esta função retorne zero        
            valor = (valor.isEmpty()) ? Metodos.valorTempCampo : valor;

            DecimalFormatSymbols dfs = new DecimalFormatSymbols(new Locale("pt", "BR"));
            DecimalFormat df = new DecimalFormat("#,##0.00", dfs);
            return df.parse(valor.replace("R$ ", "")).doubleValue();

        } catch (Exception e) {
            return 0.00;
        }
    }

    public String converteDoubleParaStringMoeda(Double valor) {
        try {
            DecimalFormatSymbols moeda = new DecimalFormatSymbols(new Locale("pt", "BR"));
            DecimalFormat df = new DecimalFormat("#,##0.00", moeda);
            String valorRetorno = "R$ " + df.format(valor);
            if (valor < 0.00) {
                valorRetorno = "-R$ " + (df.format(valor).replace("-", ""));
            }
            return valorRetorno;

        } catch (Exception e) {
            return "R$ 0,00";
        }
    }

    public Double converteStringPorcentoParaDouble(String valor) {
        try {
            DecimalFormatSymbols dfs = new DecimalFormatSymbols(new Locale("pt", "BR"));
            DecimalFormat df = new DecimalFormat("#,##0.00", dfs);
            return df.parse(valor.replace("%", "")).doubleValue();

        } catch (Exception e) {
            return 0.00;
        }
    }

    public String converteDoubleParaStringPorcento(Double valor) {
        try {
            DecimalFormatSymbols moeda = new DecimalFormatSymbols(new Locale("pt", "BR"));
            DecimalFormat df = new DecimalFormat("#,##0.00", moeda);
            return df.format(valor).replace(",00", "") + "%";

        } catch (Exception e) {
            return "0%";
        }
    }

    public String retornaDadosURL(URL url) {

        String result = new String();
        try {
            URLConnection uc;
            StringBuilder stringBuilderJson = new StringBuilder();
            uc = url.openConnection();
            uc.connect();
            uc = url.openConnection();
            uc.addRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)");
            uc.getInputStream();
            BufferedInputStream in = new BufferedInputStream(uc.getInputStream());
            int ch;
            while ((ch = in.read()) != -1) {
                stringBuilderJson.append((char) ch);
            }
            result = stringBuilderJson.toString();

        } catch (Exception e) {
            result = null;

        } finally {
            return result;
        }
    }

    public boolean validaNumeroRenavam(String renavam) {
        
        renavam = zerosAEsquerdaDaString(renavam, '0', 11);
        // Pegando como exemplo o renavam = 639884962

        // Completa com zeros a esquerda se for no padrao antigo de 9 digitos
        // renavam = 00639884962
        if (renavam.matches("^([0-9]{9})$")) {
            renavam = "00" + renavam;
        }

        // Valida se possui 11 digitos pos formatacao
        if (!renavam.matches("[0-9]{11}")) {
            return false;
        }

        // Remove o digito (11 posicao)
        // renavamSemDigito = 0063988496
        String renavamSemDigito = renavam.substring(0, 10);

        // Inverte os caracteres (reverso)
        // renavamReversoSemDigito = 69488936
        String renavamReversoSemDigito = new StringBuffer(renavamSemDigito).reverse().toString();

        int soma = 0;

        // Multiplica as strings reversas do renavam pelos numeros multiplicadores
        // para apenas os primeiros 8 digitos de um total de 10
        // Exemplo: renavam reverso sem digito = 69488936
        // 6, 9, 4, 8, 8, 9, 3, 6
        // * * * * * * * *
        // 2, 3, 4, 5, 6, 7, 8, 9 (numeros multiplicadores - sempre os mesmos [fixo])
        // 12 + 27 + 16 + 40 + 48 + 63 + 24 + 54
        // soma = 284
        for (int i = 0; i < 8; i++) {
            Integer algarismo = Integer.parseInt(renavamReversoSemDigito.substring(i, i + 1));
            Integer multiplicador = i + 2;
            soma += algarismo * multiplicador;
        }

        // Multiplica os dois ultimos digitos e soma
        soma += Character.getNumericValue(renavamReversoSemDigito.charAt(8)) * 2;
        soma += Character.getNumericValue(renavamReversoSemDigito.charAt(9)) * 3;

        // mod11 = 284 % 11 = 9 (resto da divisao por 11)
        int mod11 = soma % 11;

        // Faz-se a conta 11 (valor fixo) - mod11 = 11 - 9 = 2
        int ultimoDigitoCalculado = 11 - mod11;

        // ultimoDigito = Caso o valor calculado anteriormente seja 10 ou 11, transformo ele em 0
        // caso contrario, eh o proprio numero
        ultimoDigitoCalculado = (ultimoDigitoCalculado >= 10 ? 0 : ultimoDigitoCalculado);

        // Pego o ultimo digito do renavam original (para confrontar com o calculado)
        int digitoRealInformado = Integer.valueOf(renavam.substring(renavam.length() - 1, renavam.length()));

        // Comparo os digitos calculado e informado
        if (ultimoDigitoCalculado == digitoRealInformado) {
            return true;
        }
        return false;
    }

    public String zerosAEsquerdaDaString(String value, char c, int size) {
        String result = value;
        while (result.length() < size) {
            result = c + result;
        }
        return result;
    }

    public boolean validaEmail(String email) {
        boolean isEmailIdValid = false;
        if (email != null && email.length() > 0) {
            String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
            Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(email);
            if (matcher.matches()) {
                isEmailIdValid = true;
            }
        }
        return isEmailIdValid;
    }

    public Date atribuirDiasEmData(Date dataParametro, Integer diasAtribuido) {

        // calendario para rotinas de datas
        Calendar calendario = Calendar.getInstance();
        calendario.setTime(dataParametro);

        // achar data final
        calendario.add(Calendar.DATE, diasAtribuido);
        return calendario.getTime();

    }

    public Integer retornaPrimeiroDiaDoMes(Date date) {
        Calendar c = new GregorianCalendar();
        c.setTime(date);
        return c.getActualMinimum(Calendar.DAY_OF_MONTH);
    }

    public Integer retornaUltimoDiaDoMes(Date date) {
        Calendar c = new GregorianCalendar();
        c.setTime(date);
        return c.getActualMaximum(Calendar.DAY_OF_MONTH);
    }

    public Integer retornaDiaDaData(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        return c.get(Calendar.DATE);
    }

    public Integer retornaMesDaData(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        return c.get(Calendar.MONTH) + 1; // janeiro é 0 logo somamos mais um
    }

    public Integer retornaAnoDaData(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        return c.get(Calendar.YEAR);
    }

    public Date atribuirMesEmData(Date dataParametro, Integer mesAtribuido) {
        // calendario para rotinas de datas
        Calendar calendario = Calendar.getInstance();
        calendario.setTime(dataParametro);

        // achar data final
        calendario.add(Calendar.MONTH, mesAtribuido);
        return calendario.getTime();
    }

    public Date atribuirAnoEmData(Date dataParametro, Integer anoAtribuido) {
        // calendario para rotinas de datas
        Calendar calendario = Calendar.getInstance();
        calendario.setTime(dataParametro);

        // achar data final
        calendario.add(Calendar.YEAR, anoAtribuido);
        return calendario.getTime();
    }

    public Date removerTempoDaData(Date dataParametro) {
        try {
            Locale local = new Locale("pt", "BR");
            DateFormat formatador = new SimpleDateFormat("yyyy-MM-dd", local);
            return formatador.parse(formatador.format(dataParametro));

        } catch (ParseException e) {
            return null;
        }
    }

    public java.sql.Date dateUtilParaDateSql(Date data) {
        try {
            return new java.sql.Date(data.getTime());
        } catch (Exception e) {
            return null;
        }
    }

    public Integer geradorNumeroAleatorio() {
        Integer numero = 10000;
        try {
            Random rand = new Random();
            numero = rand.nextInt((99999 - 10000) + 1) + 10000;

        } catch (Exception e) {
            System.out.println("erro ao gerar numero rand!");

        } finally {
            return numero;
        }
    }

    public int confirmarAcaoUsuario(String titulo, String mensagem) throws Exception {
        Object[] options = {"Confirmar", "Cancelar"};
        int resposta = JOptionPane.showOptionDialog(null, mensagem, titulo, JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
        System.out.println("JOptionPane eixo: " + resposta);
        if (resposta != 0) {
            throw new Exception("");
        }
        return resposta;
    }

    public void informarAcaoUsuario(String titulo, String mensagem) {
        Object[] options = {"Ok"};
        JOptionPane.showOptionDialog(null, mensagem, titulo, JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
    }

    public int confirmarAcaoPersonalizadaUsuario(Object[] nomeBotoes, String titulo, String mensagem) throws Exception {
        int resposta = JOptionPane.showOptionDialog(null, titulo, mensagem, JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, nomeBotoes, nomeBotoes[0]);
        System.out.println("JOptionPane eixo: " + resposta);
        return resposta;
    }

    public String perguntarAcaoUsuario(String pergunta) throws Exception {
        String resposta = JOptionPane.showInputDialog(pergunta);
        System.out.println("JOptionPane resposta: " + resposta);
        if (resposta == null || resposta.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Você não respondeu a pergunta!");
            throw new Exception("");
        }
        return resposta;
    }

    public void exibirTextoEmJanela(Vector<String> linhas) {
        try {
            JOptionPane.showMessageDialog(null, new JScrollPane(new JList(linhas)));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String removerCaracteresEspeciaisDaString(String texto) {
        try {
            texto = Normalizer.normalize(texto, Normalizer.Form.NFD);
            texto = texto.replaceAll("[^\\p{ASCII}]", "");
            return texto;
        } catch (Exception e) {
            return texto;
        }
    }

    public Thread piscarObjeto(Object objeto) {
        Thread result = null;
        try {
            Color corAtencao = new Color(255, 102, 102);

            // faz o JTextField piscar
            if (objeto instanceof JTextField) {
                JTextField jTextField = (JTextField) objeto;
                result = new Thread() {
                    @Override
                    public void run() {
                        try {
                            Color corAtual = jTextField.getBackground();
                            for (int i = 1; i <= 6; i++) {
                                jTextField.setBackground(corAtencao);
                                tempoSleep(0.3);
                                jTextField.setBackground(corAtual);
                                tempoSleep(0.3);
                            }
                        } catch (Exception e) {
                            System.out.println("erro ao piscar JTextField!");
                        }
                    }
                };
            }

            // faz o JLabel piscar
            if (objeto instanceof JLabel) {
                JLabel jLabel = (JLabel) objeto;
                result = new Thread() {
                    @Override
                    public void run() {
                        try {
                            for (int i = 1; i <= 6; i++) {
                                jLabel.setVisible(false);
                                tempoSleep(0.3);
                                jLabel.setVisible(true);
                                tempoSleep(0.3);
                            }
                        } catch (Exception e) {
                            System.out.println("erro ao piscar JLabel!");
                        }
                    }
                };
            }

            // inicia a thread especifica do objeto passado no parametro
            result.start();

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            return result;
        }
    }

    public int capturaDiaDoMesDaData(Date data) {
        try {
            Calendar calendario = Calendar.getInstance();
            calendario.setTime(data);
            return calendario.get(Calendar.DAY_OF_MONTH);

        } catch (Exception e) {
            return 0;
        }
    }

}
